package com.sosnovskyi.searchservice.controller;

import com.sosnovskyi.commondomain.dto.SpecialistOutputDto;
import com.sosnovskyi.searchservice.dto.SearchSpecialistDto;
import com.sosnovskyi.searchservice.service.SpecialistSearchService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("specialists")
public class SpecialistController {
    private final SpecialistSearchService specialistSearchService;

    public SpecialistController(SpecialistSearchService specialistSearchService) {
        this.specialistSearchService = specialistSearchService;
    }

    @GetMapping
    public ResponseEntity<List<SpecialistOutputDto>> getAllSpecialists() {
        return new ResponseEntity<>(specialistSearchService.findAllSpecialists(), HttpStatus.OK);
    }

    @PostMapping("/search")
    public ResponseEntity<List<SpecialistOutputDto>> searchSpecialist(@RequestBody SearchSpecialistDto searchDto,
                                                                      @RequestParam Map<String, String> pagingParams) {
        return new ResponseEntity<>(specialistSearchService.searchSpecialists(searchDto, pagingParams), HttpStatus.OK);
    }

    @GetMapping("/recommended/{id}")
    public ResponseEntity<Page<SpecialistOutputDto>> recommendations(@PathVariable Long id,
                                                                     @RequestParam Map<String, String> pagingParams) {
        return new ResponseEntity<>(specialistSearchService.recommendedSpecialists(id, pagingParams), HttpStatus.OK);
    }

    @GetMapping("/top")
    public ResponseEntity<Map<String, List<SpecialistOutputDto>>> topSpecialists() {
        return new ResponseEntity<>(specialistSearchService.getTopSpecialists(), HttpStatus.OK);
    }

    @GetMapping("/{email}")
    public ResponseEntity<SpecialistOutputDto> getSpecialist(@PathVariable String email) {
        return new ResponseEntity<>(specialistSearchService.getSpecialist(email), HttpStatus.OK);
    }
}
