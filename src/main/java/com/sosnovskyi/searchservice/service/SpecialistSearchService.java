package com.sosnovskyi.searchservice.service;

import com.sosnovskyi.commondomain.dto.SpecialistOutputDto;
import com.sosnovskyi.commondomain.service.CommonSpecialistService;
import com.sosnovskyi.searchservice.dto.SearchSpecialistDto;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface SpecialistSearchService extends CommonSpecialistService {

    List<SpecialistOutputDto> searchSpecialists(SearchSpecialistDto searchDto,
                                                Map<String, String> pagingParams);

    Page<SpecialistOutputDto> recommendedSpecialists(Long id, Map<String, String> pagingParams);
    Map<String, List<SpecialistOutputDto>> getTopSpecialists();
    SpecialistOutputDto getSpecialist(String email);
    List<SpecialistOutputDto> findAllSpecialists();
}
