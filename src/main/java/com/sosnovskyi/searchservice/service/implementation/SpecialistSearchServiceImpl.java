package com.sosnovskyi.searchservice.service.implementation;

import com.sosnovskyi.commondomain.dto.SpecialistOutputDto;
import com.sosnovskyi.commondomain.exception.UserNotFoundException;
import com.sosnovskyi.commondomain.mapper.SpecialistMapper;
import com.sosnovskyi.commondomain.model.Category;
import com.sosnovskyi.commondomain.model.Specialist;
import com.sosnovskyi.commondomain.model.User;
import com.sosnovskyi.commondomain.service.util.PaginatorUtil;
import com.sosnovskyi.searchservice.dto.SearchSpecialistDto;
import com.sosnovskyi.searchservice.repository.SpecialistSearchRepository;
import com.sosnovskyi.searchservice.repository.UserSearchRepository;
import com.sosnovskyi.searchservice.service.SpecialistSearchService;
import com.sosnovskyi.searchservice.specification.SpecificationSpecialistUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static com.sosnovskyi.commondomain.service.util.PaginatorUtil.DEFAULT_PAGE_NUMBER;
import static com.sosnovskyi.commondomain.service.util.PaginatorUtil.DEFAULT_PAGE_SIZE;


@Service
@Transactional
public class SpecialistSearchServiceImpl implements SpecialistSearchService {

    private final SpecialistSearchRepository specialistSearchRepository;
    private final UserSearchRepository userSearchRepository;

    public SpecialistSearchServiceImpl(SpecialistSearchRepository specialistSearchRepository,
                                       UserSearchRepository userSearchRepository) {
        this.specialistSearchRepository = specialistSearchRepository;
        this.userSearchRepository = userSearchRepository;
    }

    @Override
    public List<SpecialistOutputDto> searchSpecialists(SearchSpecialistDto searchDto,
                                                       Map<String, String> pagingParams) {
        Pageable pageable = PaginatorUtil.createPageable(pagingParams);

        List<Specification<Specialist>> specifications = createSpecificationList(searchDto);
        Page<Specialist> specialists = specialistSearchRepository.findAll(
                specifications.stream().reduce(Specification.where(null), Specification::and),
                pageable);

        return specialists.stream()
                .map(SpecialistMapper::toOutputDto)
                .collect(Collectors.toList());
    }

    @Override
    public Page<SpecialistOutputDto> recommendedSpecialists(Long id, Map<String, String> pagingParams) {
        User user = userSearchRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id.toString()));

        List<Long> mostVisitedCategories = user.getLastVisitedSpecialists().stream()
                .flatMap(specialist -> specialist.getCategories().stream().map(Category::getId))
                .collect(Collectors.groupingBy(category -> category, Collectors.counting()))
                .entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .limit(3)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        int pageSize = Integer.parseInt(pagingParams.getOrDefault("pageSize", DEFAULT_PAGE_SIZE));
        int pageNumber = Integer.parseInt(pagingParams.getOrDefault("pageNumber", DEFAULT_PAGE_NUMBER));
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        if (mostVisitedCategories.isEmpty()) {
            pagingParams.put("sortBy", "rating");
            pagingParams.put("sortDirection", "DESC");
            pageable = PaginatorUtil.createPageable(pagingParams);

            return specialistSearchRepository.findAll(pageable)
                    .map(SpecialistMapper::toOutputDto);
        }

        return specialistSearchRepository
                .findRecommendedSpecialists(user.getCity(), mostVisitedCategories, pageable)
                .map(SpecialistMapper::toOutputDto);
    }

    @Override
    public Map<String, List<SpecialistOutputDto>> getTopSpecialists() {

        Sort sort = Sort.by(Sort.Direction.DESC, "rating");

        return specialistSearchRepository.findAll(sort).stream()
                .flatMap(specialist -> specialist.getCategories().stream()
                        .map(category -> Map.entry(category.getName(),
                                SpecialistMapper.toOutputDto(specialist))
                        ))
                .collect(Collectors.groupingBy(Map.Entry::getKey,
                        Collectors.mapping(Map.Entry::getValue, Collectors.toList())))
                .entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> entry.getValue().stream()
                                .limit(5)
                                .collect(Collectors.toList())
                ));
    }

    public List<Specification<Specialist>> createSpecificationList(SearchSpecialistDto dto) {
        List<Specification<Specialist>> specifications = new ArrayList<>();

//        specifications.add(SpecificationSpecialistUtils.specialistIsVerified());

        int experienceFrom = Optional.ofNullable(dto.getExperienceFrom()).orElse(0);
        int experienceTo = Optional.ofNullable(dto.getExperienceTo()).orElse(100);
        if (experienceFrom != 0 || experienceTo != 100) {
            specifications.add(SpecificationSpecialistUtils.experienceBetween(experienceFrom, experienceTo));
        }

        BigDecimal ratingFrom = Optional.ofNullable(dto.getRatingFrom()).orElse(BigDecimal.ZERO);
        BigDecimal ratingTo = Optional.ofNullable(dto.getRatingTo()).orElse(BigDecimal.TEN);
        if (!ratingFrom.equals(BigDecimal.ZERO) || !ratingTo.equals(BigDecimal.TEN)) {
            specifications.add(SpecificationSpecialistUtils.ratingBetween(ratingFrom, ratingTo));
        }

        int ageFrom = Optional.ofNullable(dto.getAgeFrom()).orElse(0);
        int ageTo = Optional.ofNullable(dto.getAgeTo()).orElse(100);
        if (ageFrom != 0 || ageTo != 100) {
            specifications.add(SpecificationSpecialistUtils.ageBetween(ageFrom, ageTo));
        }

        Optional.ofNullable(dto.getEducationType())
                .ifPresent(type -> specifications.add(SpecificationSpecialistUtils.educationTypeEqual(type)));

        Optional.ofNullable(dto.getGender())
                .ifPresent(gender -> specifications.add(SpecificationSpecialistUtils.genderEqual(gender)));

        Optional.ofNullable(dto.getCity())
                .ifPresent(city -> specifications.add(SpecificationSpecialistUtils.cityEqual(city)));

        Optional.ofNullable(dto.getCategory())
                .ifPresent(category -> specifications.add(SpecificationSpecialistUtils.categoryLike(category)));

        Optional.ofNullable(dto.getSearchPhrase())
                .filter(phrase -> phrase.trim().length() > 1)
                .ifPresent(phrase -> specifications.add(SpecificationSpecialistUtils.containsFullTextSearch(phrase)));

        return specifications;
    }

    @Override
    public SpecialistOutputDto getSpecialist(String email) {
        return specialistSearchRepository.findByEmail(email)
                .map(SpecialistMapper::toOutputDto)
                .orElseThrow(() -> new UserNotFoundException(email));
    }

    @Override
    public List<SpecialistOutputDto> findAllSpecialists() {
        return specialistSearchRepository.findAll().stream()
                .map(SpecialistMapper::toOutputDto)
                .collect(Collectors.toList());
    }
}
