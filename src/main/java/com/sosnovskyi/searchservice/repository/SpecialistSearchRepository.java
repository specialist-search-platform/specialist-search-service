package com.sosnovskyi.searchservice.repository;

import com.sosnovskyi.commondomain.model.Specialist;
import com.sosnovskyi.commondomain.repository.CommonSpecialistRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SpecialistSearchRepository
        extends CommonSpecialistRepository, JpaSpecificationExecutor<Specialist> {
    Optional<Specialist> findByEmail(String email);

    @Query(value = "SELECT distinct u.*, ROUND(u.rating) rating_exp, (u.city = :city) city_exp " +
            "FROM users u " +
            "JOIN specialist_categories sc ON u.id = sc.specialist_id " +
            "JOIN category cat ON sc.category_id = cat.id " +
            "WHERE u.user_type = 'specialist' " +
            "AND cat.id IN (:categories) " +
            "ORDER BY rating_exp DESC, city_exp DESC",
            countQuery = "SELECT COUNT(DISTINCT s.id) FROM users s " +
                    "JOIN specialist_categories sc ON s.id = sc.specialist_id " +
                    "JOIN category c ON sc.category_id = c.id " +
                    "WHERE s.user_type = 'specialist' " +
                    "AND c.id IN (:categories)",
            nativeQuery = true)
    Page<Specialist> findRecommendedSpecialists(@Param("city") String city,
                                                @Param("categories") List<Long> categories,
                                                Pageable pageable);

}
