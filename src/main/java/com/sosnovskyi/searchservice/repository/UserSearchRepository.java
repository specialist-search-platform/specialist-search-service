package com.sosnovskyi.searchservice.repository;

import com.sosnovskyi.commondomain.model.User;
import com.sosnovskyi.commondomain.repository.CommonUserRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserSearchRepository extends CommonUserRepository {
    Optional<User> findByEmail(String email);
    boolean existsByEmail(String email);
}
