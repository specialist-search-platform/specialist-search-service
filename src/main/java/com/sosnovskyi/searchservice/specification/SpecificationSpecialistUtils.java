package com.sosnovskyi.searchservice.specification;

import com.sosnovskyi.commondomain.model.EducationType;
import com.sosnovskyi.commondomain.model.Gender;
import com.sosnovskyi.commondomain.model.Specialist;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;

public class SpecificationSpecialistUtils {

    public static Specification<Specialist> containsFullTextSearch(String searchText) {
        return (root, query, criteriaBuilder) -> {
            Expression<Boolean> booleanExpression = criteriaBuilder.function(
                    "contains_full_text", Boolean.class,
                    root.get("id"),
                    criteriaBuilder.literal(searchText)
            );
            return criteriaBuilder.isTrue(booleanExpression);
        };
    }

    public static Specification<Specialist> experienceBetween(int from, int to) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.between(root.get("experience"), from, to);
    }

    public static Specification<Specialist> ratingBetween(BigDecimal from, BigDecimal to) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.between(root.get("rating"), from, to);
    }

    public static Specification<Specialist> ageBetween(Integer from, Integer to) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.between(root.get("age"), from, to);
    }

    public static Specification<Specialist> educationTypeEqual(EducationType educationType) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(findPath(root, "educationType"), educationType);
    }

    public static Specification<Specialist> specialistIsVerified() {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(findPath(root, "isVerified"), true);
    }

    public static Specification<Specialist> cityEqual(String city) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(findPath(root, "city"), city);
    }

    public static Specification<Specialist> genderEqual(Gender gender) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(findPath(root, "gender"), gender);
    }

    public static Specification<Specialist> categoryLike(String category) {

        return (root, query, criteriaBuilder) -> {
            query.distinct(true);
            return criteriaBuilder.or(
                    criteriaBuilder.equal(
                            root.join("categories", JoinType.LEFT).get("name"),
                            category),
                    criteriaBuilder.like(
                            root.join("categories", JoinType.LEFT).get("description"),
                            "%" + category + "%")
            );
        };
    }

    private static Path<?> findPath(Root<?> root, String propertyName) {
        String[] strings = propertyName.split("\\.");
        Path<?> path = root;
        for (String string : strings) {
            path = path.get(string);
        }
        return path;
    }
}
