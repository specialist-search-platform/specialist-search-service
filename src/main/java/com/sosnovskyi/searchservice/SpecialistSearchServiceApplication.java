package com.sosnovskyi.searchservice;

import com.sosnovskyi.commondomain.exception.ErrorHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(
        exclude = {
                SecurityAutoConfiguration.class
        }
)
@EntityScan(basePackages = "com.sosnovskyi.commondomain.model")
@EnableJpaRepositories
@Import(ErrorHandler.class)
public class SpecialistSearchServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpecialistSearchServiceApplication.class, args);
    }

}
