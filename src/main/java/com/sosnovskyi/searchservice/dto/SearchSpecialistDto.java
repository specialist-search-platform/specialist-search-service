package com.sosnovskyi.searchservice.dto;

import com.sosnovskyi.commondomain.model.EducationType;
import com.sosnovskyi.commondomain.model.Gender;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class SearchSpecialistDto {

    private String category;
    private String city;

    private Gender gender;
    private EducationType educationType;

    private Integer experienceFrom;
    private Integer experienceTo;

    private Integer ageFrom;
    private Integer ageTo;

    private BigDecimal ratingFrom;
    private BigDecimal ratingTo;

    private String searchPhrase;
}
